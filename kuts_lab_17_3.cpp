/* file name: kuts_lab_17_3 
*студент: Куць Ганна Олександрівна
*група : КН-1-3
*дата створення: 26.04.2022
*дата останньої зміни 27.04.2022
*Лабораторна №17
*варіант : №4
*/
#include <math.h>
#include <time.h>
#include <stdlib.h>
#include <iostream>
#include <stdio.h>
#include <windows.h>
using namespace std;
int main()
{
system("cls");
SetConsoleCP(1251);
SetConsoleOutputCP(1251);
srand(time(NULL));
int i, j, k, n, m, g;
double x=0;
cout << "Ведіть розмірність трьохвимірної матриці через пробіл = ";
cin >> n >> m >> g;
int *a;
double *s;
s = new double;
a = new int[n*m*g];
*s = 1;
if(!a)
{
printf("Помилка при виділенні пам'ятi\n");
system("pause");
return 0;
}
printf("\nМатриця a[%d][%d][%d]:\n",n, m, g);
for (i = 0; i < n; i++)
{
for (j = 0; j < m; j++)
{
for (k = 0; k < g; k++)
{
a[i*m+j*g+k] = rand()%100-50;
printf( "%5d", a[i*m+j*g+k]);
x++;
*s=*s*(a[i*m+j*g+k]);
}
cout << endl;
}
cout << endl;
}
*s=pow(*s, 1/x);
cout << "\nСереднє геометричне = " << *s << "\n";
delete [] a;
delete [] s;
system("pause");
return 0;
}

