/* file name: kuts_lab_17_1 
*студент: Куць Ганна Олександрівна
*група : КН-1-3
*дата створення: 26.04.2022
*дата останньої зміни 27.04.2022
*Лабораторна №17
*варіант : №4
*/
#include <math.h>
#include <time.h>
#include <iostream>
#include <stdio.h>
#include <windows.h>
using namespace std;
int main()
{
system("cls");
SetConsoleCP(1251);
SetConsoleOutputCP(1251);
srand(time(NULL));
int i, j=0;
int n;//розмірність масиву
cout << "Ведіть розмірність масиву = ";
cin >> n;
int *a;
a = new int [n];//виділити місце в памяті
double *s;
s = new double;
*s=0;
if(!a)
{
printf("Помилка при виділенні пам'ятi\n");
system("pause");
return 0;
}
printf("Масив a( %d): \n",n);
for (i = 0; i < n; i++)
{
a[i] = rand()%100-50;
cout << a[i] << "\t";
if(a[i]>0)
{
	*s=*s+pow(i, 2);
	j++;
}
}
*s=sqrt(*s/j);
cout<< "\nСереднє квадратичне індексів додатніх елементів = " << *s << endl;
delete [] a;
delete [] s;
system("pause");
return 0;
}

