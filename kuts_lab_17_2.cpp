/* file name: kuts_lab_17_2
*студент: Куць Ганна Олександрівна
*група : КН-1-3
*дата створення: 26.04.2022
*дата останньої зміни 27.04.2022
*Лабораторна №17
*варіант : №4
*/
#include <stdlib.h>
#include <stdio.h>
#include <iostream>
#include <windows.h>
#include <time.h>
using namespace std;
int main()
{
system("cls");
SetConsoleCP(1251);
SetConsoleOutputCP(1251);
srand(time(NULL));
int i, n;
int *a;
cout<<"Введіть кількість елементів масиву a = ";
cin>>n;
a=(int *)malloc((n*sizeof(int)));//виділяємо динамічну пам'ять
if(!a)
{
printf("Помилка при виділенні пам'ятi\n");
return 0;
}
printf("Масив a( %d): \n",n);
for (i = 0; i < n; i++)
{
a[i] = rand()%100-50;
cout << a[i] << "\t";
}
printf("\n\nПеретворений масив a( %d): \n",n);
for (i = 0; i < n; i++)
{
	if(a[i]<0)//Усі від'ємні елементи замінити їх індексами
	{
		a[i]=i;
	}
	cout << a[i] << "\t";
}
cout << endl;
//звільняємо пам'ять
free(a);
return 0;
}

