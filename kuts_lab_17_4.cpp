/* file name: kuts_lab_17_4
*студент: Куць Ганна Олександрівна
*група : КН-1-3
*дата створення: 26.04.2022
*дата останньої зміни 28.04.2022
*Лабораторна №17
*варіант : №4
*/
#include <stdlib.h>
#include <stdio.h>
#include <iostream>
#include <windows.h>
#include <time.h>
using namespace std;
int main()
{
system("cls");
SetConsoleCP(1251);
SetConsoleOutputCP(1251);
srand(time(NULL));
int i, j, n, m, k =-1, sum;
int *a, *b;
cout<<"Введіть кількість рядків матриці = ";
cin>>n;
cout<<"Введіть кількість стовбчиків = ";
cin>>m;
a=(int *)calloc(n*m,sizeof(int));//виділяємо динамічну пам'ять
if(!a)
{
printf("Помилка при виділенні пам'ятi\n");
return 0;
}
b=(int *)calloc(n,sizeof(int));//виділяємо динамічну пам'ять
if(!b)
{
printf("Помилка при виділенні пам'ятi\n");
return 0;
}
cout<<"Матриця a["<<n<<"]["<<m<<"] \n";
for (i=0; i<n; i++)
{
for (j=0; j<m; j++)
{
*(a+i*m+j)=rand()%100-50;
printf(" %3d",*(a+i*m+j));//виводимо елементи масиву
}
cout<<endl;
}
// Пошук суми
for (i=0; i<n; i++)
{
if(i%2==0)
{
k++;
b[k]=0;
}
for (j=0; j<m; j++)
{
if (*(a+i*m+j)>0 && i%2==0)
{
b[k]=b[k]+(*(a+i*m+j));
}
}
}
cout<<"\nМатриця b["<<k+1<<"] \n";
for (i=0; i<=k; i++)//виводимо елементи масиву
cout << b[i] << "  ";
cout << endl;
free(a);
return 0;
}

